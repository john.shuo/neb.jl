# 定义原子结构
struct Atom
    element::String
    position::Vector{Float64}
end

# 用于表示分子内部坐标的结构
struct InternalCoordinate
    type::Symbol        # :bond, :angle, 或 :dihedral
    atoms::Vector{Int}  # 参与该内部坐标的原子索引
end

function read_xyz(filename::String)
    atoms = Atom[]
    open(filename, "r") do file
        readline(file)  # 跳过第一行（原子总数）
        readline(file)  # 跳过第二行（注释）

        for line in eachline(file)
            fields = split(line)
            element = fields[1]
            position = parse.(Float64, fields[2:4])
            push!(atoms, Atom(element, position))
        end
    end
    return atoms
end

# 将包含多个分子结构的路径写入 XYZ 文件的函数
function write_xyz_path(filename::String, path::Vector{Vector{Atom}})
    open(filename, "w") do file
        for (idx, molecule) in enumerate(path)
            # 写入原子总数和注释行
            println(file, length(molecule))
            println(file, "Molecule number: $(idx)")

            # 写入每个原子的元素和坐标
            for atom in molecule
                println(file, "$(atom.element) $(atom.position[1]) $(atom.position[2]) $(atom.position[3])")
            end

            # 在分子之间添加空行
            println(file)
        end
    end
end

# 定义原始内部坐标的函数（需要根据分子具体实现）
function define_primitive_internal_coordinates(molecule::Vector{Atom})
    internal_coords = InternalCoordinate[]
    for i in 1:length(molecule)-1
        for j in i+1:length(molecule)
            push!(internal_coords, InternalCoordinate(:bond, [i, j]))
        end
    end
    for i in 1:length(molecule)-2
        for j in i+1:length(molecule)-1
            for k in j+1:length(molecule)
                push!(internal_coords, InternalCoordinate(:angle, [i, j, k]))
            end
        end
    end

    # 添加二面角
    for i in 1:length(molecule)-3
        for j in i+1:length(molecule)-2
            for k in j+1:length(molecule)-1
                for l in k+1:length(molecule)
                    push!(internal_coords, InternalCoordinate(:dihedral, [i, j, k, l]))
                end
            end
        end
    end   
    
    return internal_coords
end

# 构建转换矩阵 B 的函数
function build_transformation_matrix(molecule::Vector{Atom}, internal_coords::Vector{InternalCoordinate})
    n_atoms = length(molecule)
    n_coords = length(internal_coords)

    # 初始化转换矩阵 B
    B = zeros(n_coords, 3 * n_atoms)

    for (i, coord) in enumerate(internal_coords)
        # 处理键长
        if coord.type == :bond
            atom1, atom2 = molecule[coord.atoms[1]], molecule[coord.atoms[2]]
            r12 = atom2.position - atom1.position
            r12_norm = norm(r12)

            # 填充 B 矩阵对应行
            for k in 1:3
                B[i, 3 * (coord.atoms[1] - 1) + k] = -r12[k] / r12_norm
                B[i, 3 * (coord.atoms[2] - 1) + k] = r12[k] / r12_norm
            end

        # 处理键角
        elseif coord.type == :angle
            atom1, atom2, atom3 = molecule[coord.atoms[1]], molecule[coord.atoms[2]], molecule[coord.atoms[3]]
            r12 = atom2.position - atom1.position
            r23 = atom3.position - atom2.position
            r12_norm = norm(r12)
            r23_norm = norm(r23)

            cross_r12_r23 = cross(r12, r23)
            cross_norm = norm(cross_r12_r23)

            # 对每个原子计算导数
            for k in 1:3
                B[i, 3 * (coord.atoms[1] - 1) + k] = cross_r12_r23[k] / (r12_norm * cross_norm)
                B[i, 3 * (coord.atoms[2] - 1) + k] = -(dot(r12, r23) * r12[k] / (r12_norm^2 * cross_norm) + dot(r23, r12) * r23[k] / (r23_norm^2 * cross_norm))
                B[i, 3 * (coord.atoms[3] - 1) + k] = -cross_r12_r23[k] / (r23_norm * cross_norm)
            end

        # 处理二面角
        elseif coord.type == :dihedral
            atom1, atom2, atom3, atom4 = molecule[coord.atoms[1]], molecule[coord.atoms[2]], molecule[coord.atoms[3]], molecule[coord.atoms[4]]
            r12 = atom2.position - atom1.position
            r23 = atom3.position - atom2.position
            r34 = atom4.position - atom3.position

            cross_r12_r23 = cross(r12, r23)
            cross_r23_r34 = cross(r23, r34)
            cross_norm = norm(cross_r12_r23)

            # 对每个原子计算导数
            for k in 1:3
                B[i, 3 * (coord.atoms[1] - 1) + k] = -cross_r23_r34[k] / cross_norm
                B[i, 3 * (coord.atoms[2] - 1) + k] = dot(r23, r34) * cross_r12_r23[k] / cross_norm - dot(r12, r23) * cross_r23_r34[k] / cross_norm
                B[i, 3 * (coord.atoms[3] - 1) + k] = -B[i, 3 * (coord.atoms[2] - 1) + k]  # 使用前面计算的值
                B[i, 3 * (coord.atoms[4] - 1) + k] = cross_r12_r23[k] / cross_norm
            end
        end
    end

    return B
end

# 进行坐标的去局域化
function delocalize_coordinates(molecule::Vector{Atom}, internal_coords::Vector{InternalCoordinate})
    n_atoms = length(molecule)
    n_coords = length(internal_coords)

    # 构建转换矩阵 B
    B = build_transformation_matrix(molecule, internal_coords)

    # 计算 G 矩阵：G = B * B^T
    G = B * transpose(B)

    # 对 G 矩阵进行对角化
    eigenvalues, eigenvectors = eigen(G)

    # 创建去局域化坐标的集合
    delocalized_coords = []

    # 遍历特征值和特征向量，只保留非零特征值对应的特征向量
    for i in 1:length(eigenvalues)
        if eigenvalues[i] > 1e-5  # 选择一个小的阈值
            push!(delocalized_coords, eigenvectors[:, i])
        end
    end

    return delocalized_coords
end

# HDLC 到笛卡尔坐标的转换函数
function hdlc_to_cartesian(hdlc_coords, B, molecule)
    n_atoms = length(molecule)
    n_coords = size(B, 1)

    # 初始化笛卡尔坐标的变化量
    delta_cartesian = zeros(3 * n_atoms)

    # 将 HDLC 坐标变化转换为笛卡尔坐标变化
    for i in 1:n_coords
        for j in 1:(3 * n_atoms)
            delta_cartesian[j] += hdlc_coords[i] * B[i, j]
        end
    end

    # 应用变化到原始分子的笛卡尔坐标
    new_cartesian_coords = []
    for i in 1:n_atoms
        x = molecule[i].position[1] + delta_cartesian[3 * (i - 1) + 1]
        y = molecule[i].position[2] + delta_cartesian[3 * (i - 1) + 2]
        z = molecule[i].position[3] + delta_cartesian[3 * (i - 1) + 3]
        push!(new_cartesian_coords, [x, y, z])
    end

    return new_cartesian_coords
end

# HDLC 算法实现
function hdlc_interpolation(molecule_first::Vector{Atom}, molecule_last::Vector{Atom}, n_images::Int)
    # 定义原始内部坐标
    internal_coords_first = define_primitive_internal_coordinates(molecule_first)
    internal_coords_last = define_primitive_internal_coordinates(molecule_last)

    # 构建转换矩阵 B
    B_first = build_transformation_matrix(molecule_first, internal_coords_first)
    B_last = build_transformation_matrix(molecule_last, internal_coords_last)

    # 进行坐标的去局域化
    delocalized_coords_first = delocalize_coordinates(molecule_first, internal_coords_first)
    delocalized_coords_last = delocalize_coordinates(molecule_last, internal_coords_last)

    # 插值路径
    path = [deepcopy(molecule_first)]
    for i in 1:(n_images - 1)
        interpolated_molecule = deepcopy(molecule_first)
        alpha = i / n_images

        # 在 HDLC 坐标系中进行插值
        for j in 1:length(delocalized_coords_first)
            delta_q = delocalized_coords_last[j] - delocalized_coords_first[j]
            q_interpolated = delocalized_coords_first[j] + alpha * delta_q

            # 将插值后的 HDLC 坐标转换回笛卡尔坐标
            interpolated_cartesian_coords = hdlc_to_cartesian(q_interpolated, B_first, molecule_first)
            interpolated_molecule[j].position = interpolated_cartesian_coords
        end

        push!(path, interpolated_molecule)
    end
    push!(path, deepcopy(molecule_last))

    return path
end

# 示例使用
molecule_first = read_xyz("first.xyz")  # 初始分子结构
molecule_last = read_xyz("last.xyz")   # 最终分子结构
n_images = 10           # 插值图像数

# 生成 HDLC 插值路径
path = hdlc_interpolation(molecule_first, molecule_last, n_images)
